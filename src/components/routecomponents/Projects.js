import React from 'react'
import ProjCard from '../subcomponents/ProjCard'

const Projects = () => {
  return (
    <div className= "projects" id='projects'>
      <h1>Projects</h1>
      <ProjCard/>
    </div>
  )
}

export default Projects