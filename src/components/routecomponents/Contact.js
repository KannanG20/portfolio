import React, { useRef } from 'react'
import emailjs from '@emailjs/browser';
import linkedin from '../images/linkedin.png'
import whatsapp from '../images/whatsapp.png'

const Contact = () => {

  const form = useRef();
  const sendEmail = async (e) => {
    e.preventDefault();
    try {
      await emailjs.sendForm('service_of5lk0s', 'template_mrv6yp1', form.current, '88xV3HyG0OxylXnTa')
      alert("Email sent successfully")
    } catch (error) {
      console.log(error);
    }
  };
  return (
    <div className='contact' id='contact'>
      <div className='contactDetail'>
        <h1>Contact Me</h1>
        <div className='contactOpt'>
        <img src={linkedin} alt='linkedin'/>
        <a href='https://www.linkedin.com/in/kannan-g-995749257/' target='_blank'>LinkedIn</a>
        </div>
        <hr/>
        <div className='contactOpt'>
          <img src={whatsapp} alt="whatsapp"/>
      <a href='https://wa.me/9491660133' target='_blank'>Whatsapp</a>
      </div>
      <hr/>
      <button>Discord</button>
      </div>
      <div className='form'>
        <form ref={form} onSubmit={sendEmail}>
          <label>Name</label>
          <input type="text" name="user_name" required/>
          <label>Email</label>
          <input type="email" name="user_email" required/>
          <label>Feedback</label>
          <textarea name="message" />
          <input type="submit" value="Send" />
        </form>
      </div>
    </div>
  )
}

export default Contact