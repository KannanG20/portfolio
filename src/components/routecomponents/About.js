import React from 'react'
import MyDetails from '../subcomponents/MyDetails'

const About = () => {
  return (
    <div className='aboutMe' id='about'>
      <MyDetails/>
    </div>
  )
}

export default About