import React from 'react'
import cr from  '../images/cr.png'

const Footer = () => {
  return (
    <div className='footer'>
      <div>
        <img src={cr} alt="copyrights"/>
        <span>2023</span>
      </div>
    </div>
  )
}

export default Footer