import React from 'react'
import react from '../images/react.png'
import js from "../images/js.png"
import java from "../images/java.png"
import css from "../images/css.png"
import html from "../images/html.png"


const Skills = () => {
  return (
    <div className='skills'>
      <div className='react'>
        <img src={react} alt="reactlogo"/>
        <p>React Js</p>
      </div>
      <div className='js'>
        <img src={js} alt="javascriptlogo"/>
        <p>Javascript</p>
      </div>
      <div className='java'>
      <img src={java} alt="javalogo"/>
      <p>Java</p>
      </div>
      <div className='html'>
        <img src={html} alt="htmllogo"/>
        <p>Html</p>
      </div>
      <div className='css'>
        <img src={css} alt="csslogo"/>
        <p>CSS</p>
      </div>
    </div>
  )
}

export default Skills