import React from 'react'
import Navbar from '../subcomponents/Navbar'
import starfish from "../images/starfish.png"

const Home = () => {
  return (
    <div className='home' id='home'>
      <Navbar/>
      <div className='landerPage'>
        <div className='landerWrapper'>
          <h1>I am G Kannan</h1>
          <p>Frontend web developer</p>
          <a href='https://github.com/KannanG20?tab=repositories' target="_blank"><button>Github</button></a>
        </div>
      </div>
      <div className='box'></div>
      <img src={starfish} alt="starfish" id='starfish'/>
      <img src={starfish} alt="starfish" id='starfish1'/>
      <img src={starfish} alt="starfish" id='starfish2'/>
      <img src={starfish} alt="starfish" id='starfish3'/>
    </div>
  )
}

export default Home