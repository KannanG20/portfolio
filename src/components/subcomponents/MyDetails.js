import React from 'react'
import Skills from '../routecomponents/Skills'

const MyDetails = () => {
  return (
    <div className='details'>
      <h1>About Me</h1>
      <div>
        <p>I am G Kannan from India, Student of computers application. An FrontEnd web developer with modern Technologies,
          Worked on many projects which establishes the work flow of web development. Also a freelancer as web dev as well as android dev.
          To know more about me and my projects check out my Github page or contact me to communicate! 
        </p>
      </div>
      <h3>Skills</h3>
      <Skills/>
    </div>
  )
}

export default MyDetails