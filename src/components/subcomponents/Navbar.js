import React from 'react'

const Navbar = () => {
  return (
    <div className='navbar'>
      <div className='navLogo'>
      <a href='#home'><span>KannanG</span></a>
      </div>
      <div className='navLinks'>
        <a href='#about'>About</a>
        <a href='#projects'>Projects</a>
        <a href='#contact'>Contact</a>
        
      </div>  
    </div>  
  )
}

export default Navbar