import React from 'react'
import infotech from '../images/infotech.png'
import webpage from '../images/webpage.JPG'
import chatApp from '../images/chatApp.jpg'

const ProjCard = () => {
  return (
    <div className='projCard'>
      <div className='card'>
        <img src={infotech} alt="infoTech logo"/>
        <h3>Info Tech</h3>
        <p>Info Tech is a android application, which displays a Technology News</p>
        <a href='https://github.com/KannanG20/News-android-application' target="_blank"><button>Source</button></a>
      </div>
      <div className='card'>
        <img src={chatApp} alt="ChatApp"/>
        <h3>Chat App</h3>
        <p>Chat web app is a site where two persons can interact by chatting each other</p>
        <a href='https://github.com/KannanG20/News-android-application' target="_blank"><button>Source</button></a>
      </div>
      <div className='card'>
        <img src={webpage} alt="webpage"/>
        <h3>PortFolio</h3>
        <p>This Portfolio is my beginner project</p>
        <a href='https://github.com/KannanG20/First-Website' target="_blank"><button>Source</button></a>
      </div>

    </div>
  )
}

export default ProjCard