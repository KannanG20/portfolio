import './App.scss';
import Home from "./components/routecomponents/Home"
import About from "./components/routecomponents/About"
import Projects from "./components/routecomponents/Projects"
import Contact from "./components/routecomponents/Contact"
import Footer from "./components/routecomponents/Footer"

function App() {
  return (
    <div className="App">
      <Home/>
      <About/>
      <Projects/>
      <Contact/>
      <Footer/>
    </div>
  );
}

export default App;
